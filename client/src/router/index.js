import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/auth/Login'
import Register from '@/components/auth/Register'
import PasswordReset from '@/components/auth/PasswordReset'
import Home from '@/components/Home'
import Users from '@/components/Users'
import Projects from '@/components/Projects'
import Tasks from '@/components/Tasks'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/passwordReset',
      name: 'PasswordReset',
      component: PasswordReset
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/users',
      name: 'Users',
      component: Users
    },
    {
      path: '/projects',
      name: 'Projects',
      component: Projects
    },
    {
      path: '/tasks',
      name: 'Tasks',
      component: Tasks
    }
  ]
})
