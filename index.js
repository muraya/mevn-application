const express = require('express');
const logger = require('morgan')
const bodyParser = require('body-parser');

const app = express();

var users = require('./routes/users');
var projects = require('./routes/projects');
var tasks = require('./routes/tasks');

var cors = require('cors');

var port = 3000;

/**
 *  Middlewares
 */
app.use(logger('dev'))

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


 /**
  *  Routes
  */


 app.use('/api', users);
 app.use('/api', projects);
 app.use('/api', tasks);


  /**
   *  Catch 404 errors and foward them to error handler
   */

   app.use((req, res, next) => {
       const err = new Error("Not Found");
       err.status = 404;
       next(err);
   })

   /*
   * Error handler function
   */
  app.use((err, req, res, next) => {
      const error = app.get('env') === 'development' ? err : {}
      const status = err.status || 500;

      // Respond to client
      res.status(status).json({
          error: {
              message: error.message
          }
      })

      // log for development purposes
      console.error(err);

  })




// Start the server
app.listen(port, () => console.log('Todo app started on port ' + port));