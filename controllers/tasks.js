const Task = require("../model/Tasks.js");

module.exports = {
    index: async (req, res, next) => {
        try {
            const tasks = await Task.findAll();
            res.status(200).json(tasks);
        } catch (err) {
            next(err);
        }
    },

    newTask: async (req, res, next) => {
        try {
            const newTask = new Task(req.body);
            const task = await newTask.save();
            res.status(201).json(task);
        } catch (err) {
            next(err);
        }
    },

    getTask: async (req, res, next) => {
        try {
            const { taskId } = req.params;
            const task = await Task.findByPk(taskId);
            res.status(200).json(task);
        } catch (err) {
            next(err);
        }
    },

    updateTask: async (req, res, next) => {
        try {
            const { taskId } = req.params;
            const updateTask = await Task.findByIdAndUpdate(taskId);
            res.status(200).json(updateTask);
        } catch (err) {
            next(err);
        }
    },

    deleteTask: async (req, res, next) => {
        try {
            const { taskId } = new Task(req.body);
            const deleteTask = await Task.destroy(taskId);
            res.status(200).json(deleteTask);
        } catch (err) {
            next(err);
        }
    }
}