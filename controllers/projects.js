
const Project = require("../model/Projects");

module.exports = {
    index: async (req, res, next) => {
        try {
            const projects = await Project.findAll();
            res.status(200).json(projects);
        } catch (err) {
            next(err);
        }
    },

    newProject: async (req, res, next) => {
        try {
            const newProject = new Project(req.body);
            const project = await newProject.save();
            res.status(201).json(project);
        } catch (err) {
            next(err);
        }
    },

    getProject: async (req, res, next) => {
        try {
            const { projectId } = req.params;
           const user = await Project.findByPk(projectId);
            res.status(200).json(user);
        } catch (err) {
            next(err);
        }
    },

    updateProject: async (req, res, next) => {
        try {
            const { projectId } = req.params;
           const updatedProject = await Project.findByIdAndUpdate(projectId);
            res.status(200).json(updatedProject);
        } catch (err) {
            next(err);
        }
    },

    deleteProject: async (req, res, next) => {
        try {
            const { projectId } = new Project(req.body);
           const deletedProject = await Project.destroy(projectId);
            res.status(200).json(deletedProject);
        } catch (err) {
            next(err);
        }
    },

    getProjectTasks: async (req, res, next) => {
        try {
            const { projectId } = new Project(req.body);
            console.log(projectId);
           // const deletedProject = await Project.destroy(projectId);
           //  res.status(200).json(deletedProject);
        } catch (err) {
            next(err);
        }
    }
};

