const User = require("../model/Users.js");

const cors = require("cors");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

router.use(cors());

process.env.SECRET_KEY = 'secret';

module.exports = {
    registerUser: async (req, res, next) => {
        const todayDate = new Date();
        const userData = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            password: req.body.password,
            date_created: todayDate
        };

        User.findOne({
            where: {
                email: req.body.email
            }
        })
            .then(user => {
                if (!user) {
                    bcrypt.hash(req.body.password, 10, (err, hash) => {
                        userData.password = hash;
                        User.create(userData)
                            .then(user => {
                                res.send({status: user.email + " registered successfully"})
                            })
                            .catch(err => {
                                res.send(" Error : " + err)
                            })
                    })
                } else {
                    res.send({error: " User already exists : "})
                }
            })
            .catch(err => {
                res.send("Error " + err)
            });
    },

    loginUser: async (req, res, next) => {
        User.findOne({
            where: {
                email: req.body.email
            }
        })
            .then(user => {
                if (user) {
                    if (bcrypt.compareSync(req.body.password, user.password)) {
                        let token = jwt.sign(user.dataValues, process.env.SECRET_KEY, {
                            expiresIn: 1440
                        })
                        res.send(token)
                    } else {
                        res.status(400).json({error: "User does not exist"})
                    }
                }
            })
            .catch(err => {
                res.send({error: err})
            })
    },

    index: async (req, res, next) => {
        try {
            const users = await User.findAll();
            res.status(200).json(users);
        } catch (err) {
            next(err);
        }
    },

    deleteUser: async (req, res, next) => {
        try {
            const {userId} = new User(req.body);
            const deleteUser = await User.destroy(userId);
            res.status(200).json(deleteUser);
        } catch (err) {
            next(err);
        }
    }
}

