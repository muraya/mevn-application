var express = require("express");
var router = express.Router();

const UsersController = require("../controllers/users")

router.route("/auth/register")
    .post((req, res, next) => {
        UsersController.registerUser(req, res, next);
    });

router.route("/auth/login")
    .post((req, res, next) => {
        UsersController.loginUser(req, res, next);
    });

router.route("/users")
    .get((req, res, next) => {
        UsersController.index(req, res, next);
    })
    .post((req, res, next) => {
        UsersController.newUser(req, res, next);
    });

router.route("/users/:userId")
    .get(UsersController.getUser())
    .put(UsersController.updateUser())
    .delete(UsersController.deleteUser());

module.exports = router;