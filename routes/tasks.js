var express = require("express");
var router = express.Router();

const TaskController = require("../controllers/tasks")

router.route("/tasks")
    .get((req, res, next) => {
        TaskController.index(req, res, next);
    })
    .post((req, res, next) => {
        TaskController.newTask(req, res, next);
    });

router.route("/tasks/:taskId")
    .get(TaskController.getTask())
    .put(TaskController.updateTask())
    .delete(TaskController.deleteTask());

module.exports = router;