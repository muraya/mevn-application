var express = require("express");
var router = express.Router();

const ProjectsController = require("../controllers/projects")

router.route("/projects")
    .get((req, res, next) => {
        ProjectsController.index(req, res, next);
    })
    .post((req, res, next) => {
        ProjectsController.newProject(req, res, next);
    });

router.route("/projects/:projectId")
    .get(ProjectsController.getProject)
    .put(ProjectsController.updateProject)
    .delete(ProjectsController.deleteProject);
router.route("/projects/:projectId/tasks")
    .get(ProjectsController.getProjectTasks);

module.exports = router;