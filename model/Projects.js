const Sequelize = require("sequelize");
const db = require("../database/db");

module.exports = db.sequelize.define(
    "projects",
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        project_name:  {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.INTEGER,
            defaultValue: 1
        },
        date_created: {
            type: Sequelize.DATEONLY,
            defaultValue: Sequelize.NOW
        },
        date_modified: {
            type: Sequelize.DATEONLY
        },
        date_deleted: {
            type: Sequelize.DATEONLY
        }
    },
    {
        timestamps: false
    }
)


