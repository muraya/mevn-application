const Sequelize = require("sequelize");
const db = require("../database/db");

module.exports = db.sequelize.define(
    "users",
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        first_name:  {
            type: Sequelize.STRING
        },
        last_name: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.INTEGER,
            defaultValue: 1
        },
        date_created: {
            type: Sequelize.DATEONLY,
            defaultValue: Sequelize.NOW
        },
        date_modified: {
            type: Sequelize.DATEONLY
        },
        date_deleted: {
            type: Sequelize.DATEONLY
        }
    },
    {
        timestamps: false
    }
)



